package fun.app.worldnews;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.StringRes;

import com.google.android.gms.ads.MobileAds;

public class App extends Application {

    private static App app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        MobileAds.initialize(this, getString(R.string.admob_id));
    }

    public static App getApp() {
        return app;
    }

    public static void showToast(CharSequence text) {
        Toast.makeText(app, text, Toast.LENGTH_LONG).show();
    }
    public static void showToast(@StringRes int resId) {
        Toast.makeText(app, resId, Toast.LENGTH_LONG).show();
    }
}
