package fun.app.worldnews.engine;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import fun.app.worldnews.interfaces.Callback;
import fun.app.worldnews.interfaces.INewsSourceInfo;
import fun.app.worldnews.models.NewsSourceInfo;
import fun.app.worldnews.models.SourceInfoType;

public class NewsSourceManager extends SimpleDataSource<Callback<List<INewsSourceInfo>>, List<INewsSourceInfo>> {

    @NonNull
    private final List<INewsSourceInfo> cachedSourceInfo = new ArrayList<>();


    @NonNull
    public static INewsSource create(@NonNull INewsSourceInfo sourceInfo) throws IllegalArgumentException {
        if (sourceInfo.getSourceInfoType() == SourceInfoType.RSS) {
            return RssNewsSource.create(sourceInfo);
        }
        throw new IllegalArgumentException();
    }

    @NonNull
    public static List<INewsSourceInfo> getDefaultNewsSourceInfo() {
        final List<INewsSourceInfo> sourceInfoList = new ArrayList<>();
        final NewsSourceInfo sourceInfo = new NewsSourceInfo("Obozrevatel",
                "www.obozrevatel.com/rss.xml", SourceInfoType.RSS);
        sourceInfoList.add(sourceInfo);
        return sourceInfoList;
    }

    @Override
    public void load() {
        if (cachedSourceInfo.size() > 0) {
            throwResult(new ArrayList<>(cachedSourceInfo));
        } else {
            cachedSourceInfo.addAll(getDefaultNewsSourceInfo());
            throwResult(cachedSourceInfo);
        }
    }

    @Override
    public void clearCache() {
        cachedSourceInfo.clear();
    }
}
