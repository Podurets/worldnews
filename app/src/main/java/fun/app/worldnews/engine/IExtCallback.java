package fun.app.worldnews.engine;

import androidx.annotation.NonNull;

import fun.app.worldnews.interfaces.Callback;

public interface IExtCallback<Data> extends Callback<Data> {

    void onError(@NonNull String error);

}
