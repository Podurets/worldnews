package fun.app.worldnews.engine;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import fun.app.worldnews.engine.rss.RssFeedParser;
import fun.app.worldnews.interfaces.INewsSourceInfo;
import fun.app.worldnews.models.NewsInfo;
import fun.app.worldnews.models.SourceInfoType;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

class RssNewsSource extends NewsSource {

    private final OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build();

    private RssNewsSource(@NonNull INewsSourceInfo newsSourceInfo) {
        super(newsSourceInfo);
    }

    @Override
    public void requestData() {
        String url = Verifier.verifyUrl(getSourceInfo().getUrl());
        Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                throwResults(new ArrayList<NewsInfo>());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                ResponseBody body = response.body();
                final ArrayList<NewsInfo> newsInfoList = new ArrayList<>();
                if (body != null) {
                    String textResponse = body.string();
                    try {
                        RssFeedParser.RssInfo info = new RssFeedParser().parse(new StringReader(textResponse));
                        for (RssFeedParser.RssItem item : info.getRssItems()) {
                            NewsInfo newsInfo = new NewsInfo();
                            newsInfo.setTitle(item.getTitle());
                            newsInfo.setAuthor(item.getCreator());
                            newsInfo.setDescription(item.getDescription());
                            newsInfo.setUrl(item.getLink());
                            newsInfo.setUrlToImage(item.getImageUrl());
                            newsInfo.setPublishedAt(String.valueOf(item.getPubDate()));
                            newsInfoList.add(newsInfo);
                        }
                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                if (Looper.getMainLooper() != Looper.myLooper()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            throwResults(newsInfoList);
                        }
                    });
                } else {
                    throwResults(newsInfoList);
                }
            }
        });
    }

    @NonNull
    static RssNewsSource create(@NonNull INewsSourceInfo newsSourceInfo) throws IllegalArgumentException {
        if (newsSourceInfo.getSourceInfoType() == SourceInfoType.RSS) {
            return new RssNewsSource(newsSourceInfo);
        }
        throw new IllegalArgumentException("Only source infoType RSS allowed");
    }

    public static final String testRssString = "<rss version=\"2.0\">\n" +
            "    <channel>\n" +
            "        <image>\n" +
            "            <url>https://www.obozrevatel.com/assets/img/obozrevatel.png</url>\n" +
            "            <title>Последние новости в Украине за час | Свежие последние новости дня онлайн | Обозреватель</title>\n" +
            "            <link>https://www.obozrevatel.com/</link>\n" +
            "        </image>\n" +
            "        <title>Последние новости в Украине за час | Свежие последние новости дня онлайн | Обозреватель</title>\n" +
            "        <link>https://www.obozrevatel.com/</link>\n" +
            "        <description>Последние новости в Украине за час. Свежие последние новости дня онлайн. Обозреватель. Горячие темы, интересные статьи и аналитика про события в Украине и мире.</description>\n" +
            "        <item>\n" +
            "            <title>Сенсационное признание Бородая</title>\n" +
            "            <link>https://www.obozrevatel.com/society/denis-kazanskij-sensatsionnoe-priznanie-borodaya.htm</link>\n" +
            "            <description></description>\n" +
            "            <category>Общество</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/1200px-alexanderborodai.jpg' type='image/jpeg' />\n" +
            "                        <pubDate>Wed, 14 Aug 2019 17:24:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>Вернидуб категорично высказался о приходе в &quot;Динамо&quot;</title>\n" +
            "            <link>https://www.obozrevatel.com/sport/football/vernidub-kategorichno-vyiskazalsya-o-prihode-v-dinamo.htm</link>\n" +
            "            <description>Экс-тренер &quot;Зари&quot; сожалеет об увольнении Хацкевича</description>\n" +
            "            <category>Футбол</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2018/8/17/verny2.jpg' type='image/jpeg' />\n" +
            "                        <pubDate>Wed, 14 Aug 2019 17:21:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>&quot;Я опечален!&quot; Жениха Дженнифер Лопес нагло ограбили в США</title>\n" +
            "            <link>https://www.obozrevatel.com/show/people/ya-opechalen-zheniha-dzhennifer-lopes-naglo-ograbili-v-ssha.htm</link>\n" +
            "            <description>Грабители взломали машину спортсмена</description>\n" +
            "            <category>Люди</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/screenshot24.png' type='image/png' />\n" +
            "                                <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/screenshot24.png' type=\"image/png\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/screenshot314.png' type=\"image/png\" />\n" +
            "            <pubDate>Wed, 14 Aug 2019 17:20:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>ДТП на Сумской в Харькове: приговор Зайцевой и Дронову вступил в силу</title>\n" +
            "            <link>https://www.obozrevatel.com/crime/dtp-na-sumskoj-v-harkove-prigovor-zajtsevoj-i-dronovu-vstupil-v-silu.htm</link>\n" +
            "            <description>Апелляционный суд вынес решение</description>\n" +
            "            <category>Расследования</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/screenshot23.png' type='image/png' />\n" +
            "                                <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/678722953944435145410668941323411427164160n5d53c1534bb4d.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/6833820623872478082681922090303240854831104n5d53c153c6d7a.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/photo2019-08-1411-19-505d53c4a7638b8.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/photo2019-08-1412-46-225d53daf725007.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/photo2019-08-1412-46-545d53daf7e6534.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/photo2019-08-1412-53-055d53daf7eb31d.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/photo2019-08-1414-11-165d53ecd242cf9.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/photo2019-08-1415-21-045d53fd04261c7.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/image.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/232.png' type=\"image/png\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/zajtseva-dronov1.png' type=\"image/png\" />\n" +
            "            <pubDate>Wed, 14 Aug 2019 17:17:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>Зеленский резко поменял правила для электромобилей: что из этого вышло</title>\n" +
            "            <link>https://www.obozrevatel.com/green/transport/zakon-ob-elektromobilyah-daet-moralnyie-preferentsii-ekspert.htm</link>\n" +
            "            <description>Эксперты оценили нововведения</description>\n" +
            "            <category>Электротранспорт</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/filestoragetemp-11.jpg' type='image/jpeg' />\n" +
            "                                <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/elektroavtomobilukr-2.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/elektroavtomobil-1.jpg' type=\"image/jpeg\" />\n" +
            "            <pubDate>Wed, 14 Aug 2019 17:17:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>Официально: &quot;Динамо&quot; уволило Хацкевича</title>\n" +
            "            <link>https://www.obozrevatel.com/sport/football/dinamo-uvolilo-hatskevicha-s-posta-glavnogo-trenera.htm</link>\n" +
            "            <description>О его преемнике будет сообщено позже</description>\n" +
            "            <category>Футбол</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/1565779646.jpg' type='image/jpeg' />\n" +
            "                        <pubDate>Wed, 14 Aug 2019 17:17:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>&quot;Твое место на сцене!&quot; Ведущий из &quot;Слуги народа&quot; снова разозлил сеть</title>\n" +
            "            <link>https://www.obozrevatel.com/show/people/tvoe-mesto-na-stsene-veduschij-iz-slugi-naroda-snova-razozlil-set.htm</link>\n" +
            "            <description>Пользователи не хотят его видеть в политике</description>\n" +
            "            <category>Люди</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/screenshot-4.png' type='image/png' />\n" +
            "                                <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/113.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/211.jpg' type=\"image/jpeg\" />\n" +
            "            <pubDate>Wed, 14 Aug 2019 17:16:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>&quot;Мне хозяин разрешил!&quot; В магазине Киева разгорелся языковой скандал</title>\n" +
            "            <link>https://www.obozrevatel.com/kiyany/life/mne-hozyain-razreshil-v-magazine-kieva-razgorelsya-yazyikovoj-skandal.htm</link>\n" +
            "            <description>Его свидетелем стала писательница Лариса Ницой</description>\n" +
            "            <category>Жизнь столицы</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/307480.jpg' type='image/jpeg' />\n" +
            "                        <pubDate>Wed, 14 Aug 2019 17:01:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>&quot;Не можем сотрудничать&quot;: Меркель резко выступила против России</title>\n" +
            "            <link>https://www.obozrevatel.com/abroad/ne-mozhem-sotrudnichat-merkel-rezko-vyistupila-protiv-rossii.htm</link>\n" +
            "            <description>Она обсудила этот вопрос с президентом Литвы</description>\n" +
            "            <category>Мир</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/screenshot15.png' type='image/png' />\n" +
            "                                <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/gettyimages-1161621184-594x594.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/gettyimages-1161621191-594x594.jpg' type=\"image/jpeg\" />\n" +
            "            <pubDate>Wed, 14 Aug 2019 16:50:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>&quot;Пошли на х**!&quot; Фанаты &quot;Динамо&quot; жестко обратились к капитану команды - видео 18&#x2B;</title>\n" +
            "            <link>https://www.obozrevatel.com/sport/football/poshli-na-h-fanatyi-dinamo-zhestko-obratilis-k-kapitanu-komandyi-video-18.htm</link>\n" +
            "            <description>Жесткое видео с диалогом 18&#x2B;</description>\n" +
            "            <category>Футбол</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/1015858.jpg' type='image/jpeg' />\n" +
            "                        <pubDate>Wed, 14 Aug 2019 16:47:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>Байден пообещал сделать Украину приоритетом внешней политики США</title>\n" +
            "            <link>https://www.obozrevatel.com/politics/dzho-bajden-obeschaet-sdelat-ukrainu-prioritetom-vneshnej-politiki-ssha.htm</link>\n" +
            "            <description>Хочет заставить Кремль расплачиваться за непрекращающиеся атаки на международный порядок</description>\n" +
            "            <category>Политика</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/5/19/filestoragetemp-75.jpg' type='image/jpeg' />\n" +
            "                        <pubDate>Wed, 14 Aug 2019 16:47:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>Жителей Днепропетровщины напугал эксгибиционист</title>\n" +
            "            <link>https://www.obozrevatel.com/dnipro/accident/zhitelej-dnepropetrovschinyi-napugal-eksgibitsionist.htm</link>\n" +
            "            <description>Мужчина показывал свои половые органы прохожим</description>\n" +
            "            <category>Происшествия</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/exhibitionist-768x535.jpg' type='image/jpeg' />\n" +
            "                        <pubDate>Wed, 14 Aug 2019 16:41:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>В Киеве освобожденный судом грабитель &quot;терроризировал&quot; полицейскую. Фото</title>\n" +
            "            <link>https://www.obozrevatel.com/kiyany/crime/v-kieve-osvobozhdennyij-sudom-grabitel-terroriziroval-politsejskuyu-foto.htm</link>\n" +
            "            <description>За совершенное преступнику грозит немалый срок лишения свободы</description>\n" +
            "            <category>Криминал</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/29.jpg' type='image/jpeg' />\n" +
            "                                <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/291.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/30.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/312.jpg' type=\"image/jpeg\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/321.jpg' type=\"image/jpeg\" />\n" +
            "            <pubDate>Wed, 14 Aug 2019 16:40:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>&quot;Бархатный сезон&quot; на Азове: как сэкономить и максимально оздоровиться</title>\n" +
            "            <link>https://www.obozrevatel.com/society/barhatnyij-sezon-na-azove-kak-sekonomit-i-maksimalno-ozdorovitsya.htm</link>\n" +
            "            <description>&#xA0;Оbozrevatel разбирался, чем хорош отдых во второй половине августа-сентябре на побережье Азовского моря</description>\n" +
            "            <category>Travel</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/filestoragetemp-21.jpg' type='image/jpeg' />\n" +
            "                        <pubDate>Wed, 14 Aug 2019 16:39:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>Жириновский набросился на Путина из-за Зеленского</title>\n" +
            "            <link>https://www.obozrevatel.com/tv/zhirinovskij-nabrosilsya-na-putina-iz-za-zelenskogo.htm</link>\n" +
            "            <description></description>\n" +
            "            <category>Видео</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/ruprop1908142.jpg' type='image/jpeg' />\n" +
            "                        <pubDate>Wed, 14 Aug 2019 16:38:29 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>Зайцева резко изменила тактику в суде: что произошло</title>\n" +
            "            <link>https://www.obozrevatel.com/crime/zajtseva-rezko-izmenila-taktiku-v-sude-chto-proizoshlo.htm</link>\n" +
            "            <description>Защита виновницы ДТП сделала резонансные заявления</description>\n" +
            "            <category>Расследования</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/screenshot21.png' type='image/png' />\n" +
            "                                <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/screenshot310.png' type=\"image/png\" />\n" +
            "            <pubDate>Wed, 14 Aug 2019 11:57:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>ГПУ закрыла дело в отношении Новинского: у Луценко ответили</title>\n" +
            "            <link>https://www.obozrevatel.com/crime/gpu-zakryila-delo-v-otnoshenii-novinskogo-u-lutsenko-otvetili.htm</link>\n" +
            "            <description>Лариса Сарган дала пояснение </description>\n" +
            "            <category>Расследования</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/novinskij.jpg' type='image/jpeg' />\n" +
            "                        <pubDate>Wed, 14 Aug 2019 16:37:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>Работают даже ночью: Кличко отчитался о ремонте Борщаговского моста</title>\n" +
            "            <link>https://www.obozrevatel.com/kiyany/rabotayut-dazhe-nochyu-klichko-otchitalsya-o-remonte-borschagovskogo-mosta.htm</link>\n" +
            "            <description>Уже установили новые балки, которые будут основой новой проезжей части моста</description>\n" +
            "            <category>Кияни</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/glavnaya-1.jpg' type='image/jpeg' />\n" +
            "                        <pubDate>Wed, 14 Aug 2019 16:34:34 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>В Харькове прошел апелляционный суд по делу Зайцевой: все подробности</title>\n" +
            "            <link>https://www.obozrevatel.com/crime/v-harkove-nachalsya-apelyatsionnyij-sud-po-delu-zajtsevoj-vse-podrobnosti.htm</link>\n" +
            "            <description>Подсудимые не признают своей вины</description>\n" +
            "            <category>Расследования</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/t.png' type='image/png' />\n" +
            "                                <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/110.png' type=\"image/png\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/23.png' type=\"image/png\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/zajtseva.png' type=\"image/png\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/zajtseva-2.png' type=\"image/png\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/zajtseva-dronov.png' type=\"image/png\" />\n" +
            "                    <enclosure url='https://i.obozrevatel.com/gallery/2019/8/14/zajtseva-dronov-1.png' type=\"image/png\" />\n" +
            "            <pubDate>Wed, 14 Aug 2019 11:32:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "        <item>\n" +
            "            <title>Ольга Сумская стала жертвой сексуальных домогательств</title>\n" +
            "            <link>https://www.obozrevatel.com/show/people/olga-sumskaya-stala-zhertvoj-seksualnyih-domogatelstv.htm</link>\n" +
            "            <description>Актриса рассказала подробности</description>\n" +
            "            <category>Люди</category>\n" +
            "                <enclosure url='https://i.obozrevatel.com/2019/8/14/screenshot.png' type='image/png' />\n" +
            "                        <pubDate>Wed, 14 Aug 2019 16:29:00 &#x2B;0300</pubDate>\n" +
            "        </item>\n" +
            "    </channel>\n" +
            "</rss>";

}
