package fun.app.worldnews.engine;

import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;

import fun.app.worldnews.engine.rss.RssFeedParser;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class RssInfoLoader extends SimpleDataSource<IExtCallback<RssFeedParser.RssInfo>, RssFeedParser.RssInfo> {

    @Nullable
    private String url;
    @NonNull
    private final OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build();

    public void applyLoadUrl(@NonNull String url) {
        this.url = url;
    }


    @Override
    public void load() {
        if (url != null) {
            Request request = new Request.Builder().url(url).build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    throwError(e.getMessage());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    ResponseBody body = response.body();
                    final String message  = response.message();
                    RssFeedParser.RssInfo info = null;
                    if (body != null) {
                        String textResponse = body.string();
                        try {
                            info = new RssFeedParser().parse(new StringReader(textResponse));
                        } catch (XmlPullParserException e) {
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    if (Looper.getMainLooper() != Looper.myLooper()) {
                        final RssFeedParser.RssInfo finalInfo = info;
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                if(finalInfo!=null){
                                    throwResult(finalInfo);
                                } else {
                                    throwError(message);
                                }
                            }
                        });
                    } else {
                        if (info != null) {
                            throwResult(info);
                        } else {
                            throwError(message);
                        }
                    }
                }
            });
        } else {
            throwError("Missing url");
        }
    }

    @Override
    public void clearCache() {

    }

    private void throwError(@NonNull String error) {
        for (IExtCallback<RssFeedParser.RssInfo> callback : callbacks) {
            callback.onError(error);
        }
        callbacks.clear();
    }
}
