package fun.app.worldnews.engine;

import android.text.TextUtils;

import org.jsoup.Jsoup;

public class Utils {

    public static final String LINE_SEPARATOR = "\n";
    public static final String SPACE = " ";

    public static final String MP3= "mp3";
    public static final String JPG = "jpg";
    public static final String HTTP = "http";


    public static String pureText(String text) {
        if (TextUtils.isEmpty(text)) {
            return text;
        }
        return Jsoup.parse(text).wholeText().replaceAll(LINE_SEPARATOR, SPACE).trim();
    }

    public static boolean isMissingPureText(String text) {
        return TextUtils.isEmpty(pureText(text));
    }

    public static boolean isValidMp3(String url) {
        return !TextUtils.isEmpty(url) && url.endsWith(MP3) && url.startsWith(HTTP);
    }

    public static boolean isValidWebPage(String url) {
        return !TextUtils.isEmpty(url) &&
                !url.endsWith(MP3) &&
                url.startsWith(HTTP) &&
                !url.endsWith(JPG);
    }

}
