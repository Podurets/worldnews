package fun.app.worldnews.engine;

import androidx.annotation.NonNull;

public class Verifier {

    @NonNull
    public static String verifyUrl(@NonNull String url) {
        if (!(url.startsWith("http") || url.startsWith("https"))) {
            url = "https://" + url;
        }
        return url;
    }

}
