package fun.app.worldnews.engine.rss;

import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.jsoup.Jsoup;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import fun.app.worldnews.engine.Utils;

public class RssFeedParser {

    public static final String RSS = "rss";
    public static final String CHANNEL = "channel";
    public static final String TITLE = "title";
    public static final String LINK = "link";
    public static final String ITEM = "item";
    public static final String PUB_DATE = "pubDate";
    public static final String DB_CREATOR = "dc:creator";
    public static final String CREATOR = "creator";
    public static final String DESCRIPTION = "description";
    public static final String IMAGE = "image";
    public static final String AUTHOR = "author";
    public static final String URL = "url";
    public static final String ENCLOSURE = "enclosure";
    public static final String SUMMARY = "summary";

    public static final String PUBDATE_DATE_FORMAT = "EEE, dd MMM yyyy hh:mm:ss Z";

    //don't use XML namespaces
    private static final String ns = null;

    @NonNull
    private final SimpleDateFormat pubDateFormat = new SimpleDateFormat(PUBDATE_DATE_FORMAT,
            Locale.ENGLISH);

    @NonNull
    public RssInfo parse(Reader reader)
            throws XmlPullParserException, IOException, ParseException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser parser = factory.newPullParser();
        parser.setInput(reader);
        return readFeed(parser);
    }

    @NonNull
    private RssInfo readFeed(XmlPullParser parser)
            throws XmlPullParserException, IOException, ParseException {
        final RssInfo rssFeed = new RssInfo();
        final ArrayList<RssItem> entries = new ArrayList<>();
        parser.next();
        while (true) {
            if(parser.next() == XmlPullParser.END_TAG && parser.getName().equals(RSS)){
                break;
            }
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name){
                case CHANNEL:
                    while (true) {
                        if(parser.next() == XmlPullParser.END_TAG && parser.getName().equals(CHANNEL)){
                            break;
                        }
                        if (parser.getEventType() != XmlPullParser.START_TAG) {
                            continue;
                        }
                        switch (parser.getName()) {
                            case ITEM:
                                entries.add(readEntry(parser));
                                break;
                            case TITLE:
                                rssFeed.setTitle(readBasicTag(parser, TITLE));
                                break;
                            case AUTHOR:
                                rssFeed.setAuthor(readBasicTag(parser, AUTHOR));
                                break;
                            case LINK:
                                if(TextUtils.isEmpty(rssFeed.getLink())){
                                    rssFeed.setLink(readBasicTag(parser, LINK));
                                }
                                break;
                            case DESCRIPTION:
                                if(TextUtils.isEmpty(rssFeed.getDescription())){
                                    rssFeed.setDescription(readBasicTag(parser, DESCRIPTION));
                                }
                                break;
                            case IMAGE:
                                try {
                                    if (parser.getAttributeCount() > 0) {
                                        String url = readAttrib(parser, IMAGE, "href");
                                        if (!TextUtils.isEmpty(url)) {
                                            rssFeed.setImageUrl(url);
                                        }
                                    } else {
                                        String url = readImageFromTag(parser, IMAGE);
                                        if (!TextUtils.isEmpty(url)) {
                                            rssFeed.setImageUrl(url);
                                        }
                                    }
                                } catch (Exception e) {
                                    Log.e(RssFeedParser.class.getSimpleName(), "Cannot parse image", e);
                                }
                                break;
                        }
                    }
                    break;
            }
        }
        rssFeed.setRssItems(entries);
        return rssFeed;
    }

    private RssItem readEntry(XmlPullParser parser)
            throws XmlPullParserException, IOException, ParseException {
        parser.require(XmlPullParser.START_TAG, ns, ITEM);
        String title = null;
        String link = null;
        long pubDate = 0;
        String creator = null;
        String description = null;
        String imageUrl = null;
        String audioUrl = null;

        while (true) {
            if (parser.next() == XmlPullParser.END_TAG && parser.getName().equals(ITEM)) {
                break;
            }
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case TITLE: {
                    title = readBasicTag(parser, TITLE);
                    break;
                }
                case LINK:
                    link = readBasicTag(parser, LINK);
                    break;
                case PUB_DATE:
                    String pubDateText = readBasicTag(parser, PUB_DATE);
                    if (!TextUtils.isEmpty(pubDateText)) {
                        pubDate = pubDateFormat.parse(pubDateText).getTime();
                    } else {
                        pubDate = 0;
                    }
                    break;
                case "author":
                case CREATOR:
                case DB_CREATOR:
                    creator = readBasicTag(parser, name);
                    break;
                case DESCRIPTION: {
                    String parsedDescription = readBasicTag(parser, DESCRIPTION);
                    if(!Utils.isMissingPureText(parsedDescription)){
                        description = parsedDescription;
                    }
                    break;
                }
                case IMAGE:
                    try {
                        imageUrl = readAttrib(parser, IMAGE, "href");
                    } catch (Exception e) {
                        Log.e(RssFeedParser.class.getSimpleName(), "Cannot parse entry image", e);
                    }
                    break;
                case ENCLOSURE:
                    boolean isEmptyAudioUrl = TextUtils.isEmpty(audioUrl);
                    boolean isEmptyImageUrl = TextUtils.isEmpty(imageUrl);
                    if (isEmptyAudioUrl || isEmptyImageUrl) {
                        try {
                            String urlParsed = readAttrib(parser, ENCLOSURE, "url");
                            if (!TextUtils.isEmpty(urlParsed)) {
                                if ((urlParsed.endsWith("mp3") || urlParsed.endsWith("wave"))) {
                                    if (isEmptyAudioUrl) {
                                        audioUrl = urlParsed;
                                    }
                                } else if (urlParsed.endsWith("jpg") || urlParsed.endsWith("jpeg") || urlParsed.endsWith("png")) {
                                    if (isEmptyImageUrl) {
                                        imageUrl = urlParsed;
                                    }
                                } else if (urlParsed.contains("mp3")) {
                                    urlParsed = urlParsed.substring(0,
                                            urlParsed.indexOf("mp3") + 3);
                                    if (urlParsed.startsWith("http")) {
                                        if (isEmptyAudioUrl) {
                                            audioUrl = urlParsed;
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.e(RssFeedParser.class.getSimpleName(), "Cannot parse entry " + ENCLOSURE, e);
                        }
                    }
                    break;
                case "encoded":
                    String prefix = parser.getPrefix();
                    if ("content".equals(prefix)) {
                        String value = readBasicTag(parser, "encoded");

                        if (TextUtils.isEmpty(audioUrl)) {
                            if (!TextUtils.isEmpty(value)) {
                                int index = value.indexOf(".mp3");
                                String url = null;
                                if (index > 0) {
                                    url = value.substring(0, index + 4);
                                    if (!url.startsWith("http")) {
                                        int start = url.lastIndexOf("http");
                                        if (start > 0) {
                                            url = url.substring(start);
                                        } else {
                                            url = null;
                                        }
                                    }
                                }
                                if (!TextUtils.isEmpty(url)) {
                                    audioUrl = url;
                                }
                            }
                        }

                        if (TextUtils.isEmpty(imageUrl)) {
                            if (!TextUtils.isEmpty(value)) {
                                int index = value.indexOf(".jpg");
                                String url = null;
                                if (index > 0) {
                                    url = value.substring(0, index + 4);
                                    if (!url.startsWith("http")) {
                                        int start = url.lastIndexOf("http");
                                        if (start > 0) {
                                            url = url.substring(start);
                                        } else {
                                            url = null;
                                        }
                                    }
                                }
                                if (!TextUtils.isEmpty(url)) {
                                    imageUrl = url;
                                }
                            }
                        }
                        if (TextUtils.isEmpty(imageUrl)) {
                            if (!TextUtils.isEmpty(value)) {
                                int index = value.indexOf(".png");
                                String url = null;
                                if (index > 0) {
                                    url = value.substring(0, index + 4);
                                    if (!url.startsWith("http")) {
                                        int start = url.lastIndexOf("http");
                                        if (start > 0) {
                                            url = url.substring(start);
                                        } else {
                                            url = null;
                                        }
                                    }
                                }
                                if (!TextUtils.isEmpty(url)) {
                                    imageUrl = url;
                                }
                            }
                        }
                        if (TextUtils.isEmpty(imageUrl)) {
                            if (!TextUtils.isEmpty(value)) {
                                int index = value.indexOf(".jpeg");
                                String url = null;
                                if (index > 0) {
                                    url = value.substring(0, index + 5);
                                    if (!url.startsWith("http")) {
                                        int start = url.lastIndexOf("http");
                                        if (start > 0) {
                                            url = url.substring(start);
                                        } else {
                                            url = null;
                                        }
                                    }
                                }
                                if (!TextUtils.isEmpty(url)) {
                                    imageUrl = url;
                                }
                            }
                        }
                    }

                    break;
                case SUMMARY: {
                    if (Utils.isMissingPureText(description)) {
                        String parsedDescription = readBasicTag(parser, SUMMARY);
                        if (!Utils.isMissingPureText(parsedDescription)) {
                            description = parsedDescription;
                        }
                    }
                    break;
                }
            }
        }
        RssItem rssItem = new RssItem();
        rssItem.setTitle(title);
        rssItem.setLink(link);
        rssItem.setPubDate(pubDate);
        rssItem.setCreator(creator);
        rssItem.setDescription(description);
        rssItem.setImageUrl(imageUrl);
        rssItem.setAudioUrl(audioUrl);
        return rssItem;
    }

    private String readBasicTag(XmlPullParser parser, String tag)
            throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, tag);
        String result = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, tag);
        return result;
    }

    public String readImageFromTag(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
        String result = null;
        boolean isSkipped = false;
        while (true) {
            if (parser.next() == XmlPullParser.END_TAG && parser.getName().equals(tag)) {
                break;
            }
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            String value = readBasicTag(parser, name);
            switch (name) {
                case URL: {
                    result = value;
                    if(!TextUtils.isEmpty(result)){
                        result = result.trim();
                        isSkipped = !TextUtils.isEmpty(result);
                    }
                    break;
                }
            }
            if(isSkipped){
                break;
            }
        }
        return result;
    }

    private String readAttrib(XmlPullParser parser, @NonNull String tag, @NonNull String attribName) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, tag);
        String link = parser.getAttributeValue(null, attribName);
        parser.nextTag();
        //parser.require(XmlPullParser.END_TAG, ns, tag);
        return link;
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = null;
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    public static class RssItem implements Serializable {

        public static final String DESCRIPTION_TEXT_START_MARK = "<!--TEnd-->";
        public static final String DESCRIPTION_TEXT_START_ALTERNATIVE_MARK = "alt='";

        @Nullable
        private String title;

        @Nullable
        private String link;

        @Nullable
        private String description;

        @Nullable
        private String category;

        @Nullable
        private String creator;

        private long pubDate;

        @Nullable
        private String imageUrl;

        @Nullable
        private String audioUrl;

        ///// temp values
        @Nullable
        private String tempImageUrl;

        @Nullable
        private String tempProcessedDescription;

        @Nullable
        public String getTitle() {
            return title;
        }

        public void setTitle(@Nullable String title) {
            this.title = title;
        }

        @Nullable
        public String getLink() {
            return link;
        }

        public void setLink(@Nullable String link) {
            this.link = link;
        }

        @Nullable
        public String getDescription() {
            return description;
        }

        public void setDescription(@Nullable String description) {
            this.description = description;
            tempImageUrl = null;
            tempProcessedDescription = null;
        }

        public void setImageUrl(@Nullable String imageUrl) {
            this.imageUrl = imageUrl;
        }

        @Nullable
        public String getCategory() {
            return category;
        }

        public void setCategory(@Nullable String category) {
            this.category = category;
        }

        @Nullable
        public String getCreator() {
            return creator;
        }

        public void setCreator(@Nullable String creator) {
            this.creator = creator;
        }

        public long getPubDate() {
            return pubDate;
        }

        public void setPubDate(long pubDate) {
            this.pubDate = pubDate;
        }

        @Nullable
        public String getAudioUrl() {
            return audioUrl;
        }

        public void setAudioUrl(@Nullable String audioUrl) {
            this.audioUrl = audioUrl;
        }

        @Nullable
        public String getImageUrl() {
            if (TextUtils.isEmpty(imageUrl)) {
                if (TextUtils.isEmpty(tempImageUrl)) {
                    if (!TextUtils.isEmpty(description)) {
                        int endIndex = description.indexOf(Utils.JPG);
                        if (endIndex > 0) {
                            int startIndex = description.indexOf(Utils.HTTP);
                            if (startIndex >= 0) {
                                tempImageUrl = description.substring(startIndex, endIndex + Utils.JPG.length());
                            }
                        }
                    }
                }
                return tempImageUrl;
            }
            return imageUrl;
        }

        @Nullable
        public String getPossibleAudioUrl() {
            if (!TextUtils.isEmpty(getAudioUrl())) {
                return getAudioUrl();
            }
            if (Utils.isValidMp3(link)) {
                return link;
            }
            return null;
        }

        @Nullable
        public String getProcessedDescriptionText() {
            if (TextUtils.isEmpty(tempProcessedDescription)) {
                if (!TextUtils.isEmpty(description)) {

                    int startIndex = description.indexOf(DESCRIPTION_TEXT_START_MARK);
                    if (startIndex > 0 && description.length() > startIndex + DESCRIPTION_TEXT_START_MARK.length() + 1) {
                        tempProcessedDescription = Jsoup.parse(description.
                                substring(startIndex + DESCRIPTION_TEXT_START_MARK.length(), description.length())).text();
                    } else {
                        startIndex = description.indexOf(DESCRIPTION_TEXT_START_ALTERNATIVE_MARK);
                        if (startIndex > 0) {
                            int endIndex = description.indexOf("' title", startIndex);
                            if (endIndex < startIndex) {
                                endIndex = description.indexOf("'", startIndex);
                            }
                            if (endIndex > startIndex) {
                                tempProcessedDescription = Jsoup.parse(description.
                                        substring(startIndex + DESCRIPTION_TEXT_START_ALTERNATIVE_MARK.length(), endIndex)).text();
                            }
                        }
                    }
                    if (tempProcessedDescription != null) {
                        tempProcessedDescription = tempProcessedDescription.replace("]]>", "").trim();
                    }
                }
            }
            return tempProcessedDescription;
        }
    }

    public static class RssInfo implements Serializable {

        private String title;

        private String link;

        private String description;

        private String imageUrl;

        private String author;

        private ArrayList<RssItem> rssItems = new ArrayList<>();

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public ArrayList<RssItem> getRssItems() {
            return rssItems;
        }

        public void setRssItems(List<RssItem> rssItems) {
            this.rssItems.clear();
            this.rssItems.addAll(rssItems);
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        @Nullable
        public RssItem getFreshestRssItem(){
            RssItem freshItem = null;
            if (rssItems != null) {
                for (RssItem rssItem : rssItems) {
                    if (freshItem != null) {
                        if (rssItem.getPubDate() > freshItem.getPubDate()) {
                            freshItem = rssItem;
                        }
                    } else {
                        freshItem = rssItem;
                    }
                }
            }
            return freshItem;
        }

    }

}
