package fun.app.worldnews.engine;

import androidx.annotation.NonNull;

import java.util.HashSet;

import fun.app.worldnews.interfaces.Callback;
import fun.app.worldnews.interfaces.IDataSource;

public abstract class SimpleDataSource<Observer extends Callback<Data>, Data> implements IDataSource<Data> {

    @NonNull
    protected final HashSet<Observer> callbacks = new HashSet<>();

    public void subscribe(@NonNull Observer callback) {
        callbacks.add(callback);
    }

    public void unsubscribe(@NonNull Observer callback) {
        callbacks.remove(callback);
    }

    public void throwResult(Data data) {
        for (Observer callback : callbacks) {
            callback.onResult(data);
        }
        callbacks.clear();
    }

}
