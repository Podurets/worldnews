package fun.app.worldnews.engine;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import fun.app.worldnews.interfaces.Callback;
import fun.app.worldnews.interfaces.INewsSourceInfo;
import fun.app.worldnews.models.NewsInfo;

abstract class NewsSource implements INewsSource {

    @NonNull
    private final HashSet<Callback<List<NewsInfo>>> callbacksSet = new HashSet<>();

    @NonNull
    private final INewsSourceInfo newsSourceInfo;

    NewsSource(@NonNull INewsSourceInfo newsSourceInfo) {
        this.newsSourceInfo = newsSourceInfo;
    }

    @NonNull
    @Override
    public INewsSourceInfo getSourceInfo() {
        return newsSourceInfo;
    }

    protected void throwResults(@NonNull List<NewsInfo> newsInfo){
        for (Callback<List<NewsInfo>> callback : callbacksSet) {
            callback.onResult(new ArrayList<>(newsInfo));
        }
    }

    @Override
    public void subscribe(@NonNull Callback<List<NewsInfo>> callback) {
        callbacksSet.add(callback);
    }

    @Override
    public void unsubscribe(@NonNull Callback<List<NewsInfo>> callback) {
        callbacksSet.remove(callback);
    }

    @Override
    public void destroy() {
        callbacksSet.clear();
    }
}
