package fun.app.worldnews.engine;

import androidx.annotation.NonNull;

import java.util.List;

import fun.app.worldnews.interfaces.Callback;
import fun.app.worldnews.interfaces.INewsSourceInfo;
import fun.app.worldnews.models.NewsInfo;

public interface INewsSource {

    @NonNull
    INewsSourceInfo getSourceInfo();

    void requestData();

    void subscribe(@NonNull Callback<List<NewsInfo>> callback);

    void unsubscribe(@NonNull Callback<List<NewsInfo>> callback);

    void destroy();

}
