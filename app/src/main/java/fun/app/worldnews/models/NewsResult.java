package fun.app.worldnews.models;

import java.util.List;

public class NewsResult extends Result<List<NewsInfo>> {

    public NewsResult(boolean isSuccess) {
        super(isSuccess);
    }

    public NewsResult(boolean isSuccess, String message) {
        super(isSuccess);
        this.message = message;
    }
}
