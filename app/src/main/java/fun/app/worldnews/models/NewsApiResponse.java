package fun.app.worldnews.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;

public class NewsApiResponse {

    private String status;
    private int totalResults;
    @NonNull
    @SerializedName("articles")
    private List<NewsInfo> newsInfoList = new ArrayList<>();

    @NonNull
    public List<NewsInfo> getNewsInfoList() {
        return newsInfoList;
    }
}
