package fun.app.worldnews.models;

import android.os.Parcel;

import androidx.annotation.NonNull;

import java.util.Objects;

import fun.app.worldnews.interfaces.INewsSourceInfo;

public class NewsSourceInfo implements INewsSourceInfo {

    private String title;
    @NonNull
    private final String url;
    @NonNull
    private final SourceInfoType sourceInfoType;
    private final int id;

    public NewsSourceInfo(String title, @NonNull String url, @NonNull SourceInfoType sourceInfoType) {
        this.title = title;
        this.url = url;
        this.sourceInfoType = sourceInfoType;
        id = url.hashCode();
    }

    @NonNull
    @Override
    public SourceInfoType getSourceInfoType() {
        return sourceInfoType;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @NonNull
    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.url);
        dest.writeInt(this.sourceInfoType.ordinal());
        dest.writeInt(this.id);
    }

    protected NewsSourceInfo(Parcel in) {
        this.title = in.readString();
        this.url = Objects.requireNonNull(in.readString());
        int tmpSourceInfoType = in.readInt();
        this.sourceInfoType = Objects.requireNonNull(tmpSourceInfoType == -1 ? null : SourceInfoType.values()[tmpSourceInfoType]);
        this.id = in.readInt();
    }

    public static final Creator<NewsSourceInfo> CREATOR = new Creator<NewsSourceInfo>() {
        @Override
        public NewsSourceInfo createFromParcel(Parcel source) {
            return new NewsSourceInfo(source);
        }

        @Override
        public NewsSourceInfo[] newArray(int size) {
            return new NewsSourceInfo[size];
        }
    };
}
