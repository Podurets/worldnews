package fun.app.worldnews.models;

import android.content.Context;

import androidx.annotation.NonNull;
import fun.app.worldnews.R;

public enum NewsCategory {

    ALL, BUSINESS, ENTERTAINMENT, GENERAL, HEALTH, SCIENCE, SPORTS, TECHNOLOGY;


    public static CharSequence getText(@NonNull NewsCategory category, @NonNull Context context){
        switch (category){
            case ALL:
                return context.getString(R.string.all);
            case BUSINESS:
                return context.getString(R.string.business);
            case ENTERTAINMENT:
                return context.getString(R.string.entertainment);
            case GENERAL:
                return context.getString(R.string.general);
            case HEALTH:
                return context.getString(R.string.health);
            case SCIENCE:
                return context.getString(R.string.science);
            case SPORTS:
                return context.getString(R.string.sports);
            case TECHNOLOGY:
                return context.getString(R.string.technology);
        }
        return "";
    }
}
