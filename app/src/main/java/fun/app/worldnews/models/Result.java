package fun.app.worldnews.models;

public abstract class Result<Result> {

    protected String message;
    protected int code;
    private final boolean isSuccess;

    private Result result;

    protected Result(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void applyResult(Result result) {
        this.result = result;
    }

    public Result getResult() {
        return result;
    }

    public boolean isSuccess() {
        return isSuccess;
    }
}
