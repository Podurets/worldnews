package fun.app.worldnews.interfaces;

public interface IDataSource<Data> {

    void load();

    void clearCache();

}
