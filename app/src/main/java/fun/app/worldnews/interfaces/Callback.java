package fun.app.worldnews.interfaces;

public interface Callback<Result> {

    void onResult(Result result);

}
