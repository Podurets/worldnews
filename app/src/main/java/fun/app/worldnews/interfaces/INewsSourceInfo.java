package fun.app.worldnews.interfaces;

import android.os.Parcelable;

import androidx.annotation.NonNull;

import fun.app.worldnews.models.SourceInfoType;

public interface INewsSourceInfo extends Parcelable {

    @NonNull
    SourceInfoType getSourceInfoType();

    int getId();

    String getTitle();

    @NonNull
    String getUrl();

}
