package fun.app.worldnews.vm;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import fun.app.worldnews.engine.INewsSource;
import fun.app.worldnews.engine.NewsSourceManager;
import fun.app.worldnews.interfaces.Callback;
import fun.app.worldnews.interfaces.INewsSourceInfo;
import fun.app.worldnews.models.NewsInfo;

public class NewsListViewModel extends AViewModel<INewsView> {

    @Nullable
    private INewsSource newsSource;

    @Override
    public void attachView(@NonNull INewsView view) {
        super.attachView(view);
        tryRequestData();
    }

    public void applyNewsSourceInfo(@NonNull INewsSourceInfo sourceInfo) {
        if (newsSource == null || newsSource.getSourceInfo().getId() != sourceInfo.getId()) {
            release(newsSource);
            newsSource = NewsSourceManager.create(sourceInfo);
            newsSource.subscribe(newsCallback);
            tryRequestData();
        }
    }

    private void tryRequestData() {
        if (newsSource != null) {
            newsSource.requestData();
        }
    }

    private Callback<List<NewsInfo>> newsCallback = new Callback<List<NewsInfo>>() {
        @Override
        public void onResult(List<NewsInfo> newsInfo) {
            INewsView view = view();
            if (view != null) {
                view.draw(newsInfo);
            }
        }
    };

    @Override
    protected void onCleared() {
        super.onCleared();
        release(newsSource);
    }

    private void release(INewsSource newsSource) {
        if (newsSource != null) {
            newsSource.unsubscribe(newsCallback);
            newsSource.destroy();
        }
    }

}
