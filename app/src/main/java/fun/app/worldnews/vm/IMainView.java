package fun.app.worldnews.vm;

import androidx.annotation.NonNull;
import fun.app.worldnews.models.NewsCategory;
import fun.app.worldnews.models.NewsResult;

public interface IMainView extends IView {

    void drawNews(@NonNull NewsCategory newsCategory, @NonNull NewsResult newsResult);

}
