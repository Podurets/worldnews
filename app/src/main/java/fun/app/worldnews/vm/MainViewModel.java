package fun.app.worldnews.vm;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import fun.app.worldnews.interfaces.Callback;
import fun.app.worldnews.models.NewsApiResponse;
import fun.app.worldnews.models.NewsCategory;
import fun.app.worldnews.models.NewsResult;
import fun.app.worldnews.repo.NewsRepository;
import retrofit2.Call;

public class MainViewModel extends AViewModel<IMainView> {

    private static final long LOAD_NEWS_DELAY = 60_000 * 5;

    private final NewsRepository repository = new NewsRepository();
    @NonNull
    private NewsCategory newsCategory = NewsCategory.ALL;
    @Nullable
    private NewsResult loadedNewsResult;
    private long loadedTime;
    private boolean isLoading;
    @Nullable
    private Call<NewsApiResponse> activeCall;

    public void forceLoadNews() {
        loadedTime = 0;
        loadNews();
    }

    public void loadNews() {
        if (loadedNewsResult != null && loadedNewsResult.isSuccess() &&
                System.currentTimeMillis() - loadedTime < LOAD_NEWS_DELAY) {
            throwNewsResult(loadedNewsResult);
            return;
        }
        if (isLoading) {
            return;
        }
        isLoading = true;
        activeCall = repository.loadNews(newsCategory, callback);
    }

    public void loadNews(@NonNull NewsCategory category) {
        this.newsCategory = category;
        loadedNewsResult = null;
        if (activeCall != null && !activeCall.isCanceled()) {
            activeCall.cancel();
            activeCall = null;
        }
        isLoading = false;
        loadNews();
    }

    private void throwNewsResult(@NonNull NewsResult newsResult) {
        IMainView mainView = view();
        if (mainView != null) {
            mainView.drawNews(newsCategory, newsResult);
        }
    }

    @NonNull
    public NewsCategory getNewsCategory(){
        return newsCategory;
    }

    private Callback<NewsResult> callback = new Callback<NewsResult>() {
        @Override
        public void onResult(NewsResult newsResult) {
            isLoading = false;
            loadedNewsResult = newsResult;
            loadedTime = System.currentTimeMillis();
            if (loadedNewsResult == null) {
                loadedNewsResult = new NewsResult(false);
                loadedNewsResult.setMessage("Error load");
            }
            activeCall = null;
            throwNewsResult(loadedNewsResult);
        }
    };
}
