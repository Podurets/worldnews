package fun.app.worldnews.vm;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModel;

public abstract class AViewModel<View extends IView> extends ViewModel {

    @Nullable
    private View view;

    public void attachView(@NonNull View view){
        this.view = view;
    }

    public void detachView(){
        view = null;
    }

    @Nullable
    protected View view(){
        return view;
    }

    @Override
    protected void onCleared() {
        detachView();
    }
}
