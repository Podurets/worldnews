package fun.app.worldnews.vm;

import androidx.annotation.NonNull;

import java.util.List;

import fun.app.worldnews.models.NewsInfo;

public interface INewsView extends IView {

 void draw(@NonNull List<NewsInfo> newsInfo);

}
