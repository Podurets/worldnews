package fun.app.worldnews.vm;

import android.content.Context;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import fun.app.worldnews.models.NewsResult;

public interface IFilteredNewsView extends IView {

    void drawFilters(@NonNull List<String> filters);
    void addDrawFilter(@NonNull String filter);
    void drawNews(@NonNull NewsResult newsResult);
    @Nullable
    Context getContext();

}
