package fun.app.worldnews.vm;

import android.content.Context;

import java.util.ArrayList;
import java.util.Calendar;

import androidx.annotation.NonNull;
import fun.app.worldnews.interfaces.Callback;
import fun.app.worldnews.models.NewsResult;
import fun.app.worldnews.repo.AppDataRepository;
import fun.app.worldnews.repo.NewsRepository;

public class FilteredNewsViewModel extends AViewModel<IFilteredNewsView> {

    private ArrayList<String> filters = new ArrayList<>();
    private final NewsRepository newsRepository = new NewsRepository();
    private boolean isLoading;

    public FilteredNewsViewModel() {
    }

    @Override
    public void attachView(@NonNull IFilteredNewsView view) {
        super.attachView(view);
        Context context = view.getContext();
        if (context != null) {
            loadFilters(context);
        }
    }

    @Override
    public void detachView() {
        IFilteredNewsView view = view();
        if (view != null) {
            Context context = view.getContext();
            if (context != null) {
                AppDataRepository.saveFilters(context, filters);
            }
        }
        super.detachView();
    }

    private void loadFilters(@NonNull Context context) {
        filters.addAll(AppDataRepository.getSavedFilters(context));
    }

    public void removeFilter(@NonNull String filter) {
        if (filters.remove(filter)) {
            obtainFilters();
            loadNews();
        }
    }

    public void obtainFilters() {
        if (view() != null) {
            view().drawFilters(new ArrayList<>(filters));
        }
    }

    public void addFilter(@NonNull String filter) {
        if (!filters.contains(filter)) {
            if (filters.add(filter)) {
                if (view() != null) {
                    view().addDrawFilter(filter);
                }
                loadNews();
            }
        }
    }


    public void loadNews() {
        if (isLoading) {
            return;
        }
        isLoading = true;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -2);
        newsRepository.loadNews(new ArrayList<>(filters), calendar.getTimeInMillis(),  callback);
    }

    private void throwNewsResult(@NonNull NewsResult newsResult) {
        IFilteredNewsView newsView = view();
        if (newsView != null) {
            newsView.drawNews(newsResult);
        }
    }

    private Callback<NewsResult> callback = new Callback<NewsResult>() {
        @Override
        public void onResult(NewsResult newsResult) {
            isLoading = false;
            if (newsResult == null) {
                newsResult = new NewsResult(false);
                newsResult.setMessage("Error load");
            }
            throwNewsResult(newsResult);
        }
    };

}
