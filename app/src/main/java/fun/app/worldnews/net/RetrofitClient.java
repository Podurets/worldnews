package fun.app.worldnews.net;

import androidx.annotation.NonNull;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    @NonNull
    private final IRetrofitClient service;

    private static RetrofitClient client;

    @NonNull
    public static RetrofitClient get() {
        RetrofitClient localClient = client;
        if (localClient == null) {
            synchronized (RetrofitClient.class) {
                localClient = client;
                if (localClient == null) {
                    synchronized (RetrofitClient.class) {
                        localClient = client = new RetrofitClient();
                    }
                }
            }
        }
        return localClient;
    }

    private RetrofitClient (){
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://newsapi.org/")
                .build();
        service = retrofit.create(IRetrofitClient.class);
    }

    @NonNull
    public IRetrofitClient service(){
        return service;
    }
}
