package fun.app.worldnews.net;

import androidx.annotation.NonNull;
import fun.app.worldnews.Constants;
import fun.app.worldnews.models.NewsApiResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface IRetrofitClient {

    /* https://newsapi.org/v2/top-headlines?country=ua&apiKey=b0d86a4f2a8e4043aee20c1c7e2e6a41&pageSize=100 */

    @GET("v2/top-headlines?pageSize=" + Constants.NEWS_ON_PAGE + "&apiKey=" + Constants.NEWS_API_ACCESS_KEY)
    Call<NewsApiResponse> topHeadLines(@Query("country") String country);

    @GET("v2/top-headlines?pageSize=" + Constants.NEWS_ON_PAGE + "&apiKey=" + Constants.NEWS_API_ACCESS_KEY)
    Call<NewsApiResponse> topHeadLines(@Query("country") String country, @Query("category") @NonNull String category);

    /*https://newsapi.org/v2/everything?q=%D1%83%D0%BA%D1%80%D0%B0%D0%B8%D0%BD%D0%B0%20AND%20%D0%BF%D1%83%D1%82%D0%B8%D0%BD&apiKey=b0d86a4f2a8e4043aee20c1c7e2e6a41&pageSize=100&from=2019-03-01*/
    @GET("v2/everything?pageSize=" + Constants.NEWS_ON_PAGE + "&apiKey=" + Constants.NEWS_API_ACCESS_KEY)
    Call<NewsApiResponse> getTopicNews(@Query(value = "q", encoded = true) String query, @Query("from") @NonNull String from);

    @GET
    Call<String> loadUrl(@Url String url);

}
