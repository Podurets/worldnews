package fun.app.worldnews.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fun.app.worldnews.Constants;
import fun.app.worldnews.R;
import fun.app.worldnews.adapters.ClickHolder;
import fun.app.worldnews.adapters.NewsAdapter;
import fun.app.worldnews.adapters.SpacesItemDecoration;
import fun.app.worldnews.interfaces.INewsSourceInfo;
import fun.app.worldnews.models.NewsInfo;
import fun.app.worldnews.utils.Utils;
import fun.app.worldnews.vm.INewsView;
import fun.app.worldnews.vm.NewsListViewModel;

public class NewsListActivity extends AppCompatActivity implements INewsView {

    private NewsListViewModel newsListViewModel;
    @NonNull
    private final NewsAdapter adapter = new NewsAdapter();

    public static void startNewsListActivity(@NonNull Context context, @NonNull INewsSourceInfo sourceInfo) {
        Intent intent = new Intent(context, NewsListActivity.class);
        intent.putExtra(Constants.INTENT_KEY, sourceInfo);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_info_layout);
        final RecyclerView recyclerViewNews = findViewById(R.id.recyclerViewNews);
        recyclerViewNews.setHasFixedSize(false);
        recyclerViewNews.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        final ColorDrawable drawable = new ColorDrawable();
        drawable.setColor(Color.BLACK);
        drawable.setBounds(0, 0, 1, 5);
        dividerItemDecoration.setDrawable(drawable);
        recyclerViewNews.addItemDecoration(dividerItemDecoration);
        recyclerViewNews.addItemDecoration(new SpacesItemDecoration((int) Utils.dpToPx(5)));
        recyclerViewNews.setAdapter(adapter);
        adapter.setClickListenerAdapter(new ClickHolder.ClickListener() {
            @Override
            public void onClick(int position) {
                NewsInfo newsInfo = adapter.getNewsInfo(position);
                if (newsInfo != null && newsInfo.getUrl() != null) {
                    Intent intent = new Intent(NewsListActivity.this, BrowserActivity.class);
                    intent.putExtra(Constants.TITLE_KEY, newsInfo.getTitle());
                    intent.putExtra(Constants.URL_KEY, newsInfo.getUrl());
                    RecyclerView.ViewHolder viewHolder = recyclerViewNews.findViewHolderForAdapterPosition(position);
                    if (viewHolder instanceof NewsAdapter.Holder) {
                        startActivity(intent,
                                ActivityOptionsCompat.makeSceneTransitionAnimation(NewsListActivity.this,
                                        ((NewsAdapter.Holder) viewHolder).getTransitionView(),
                                        Constants.TRANSITION_NAME)
                                        .toBundle());
                    } else {
                        startActivity(intent);
                    }
                }
            }
        });

        final INewsSourceInfo sourceInfo = getIntent().getParcelableExtra(Constants.INTENT_KEY);
        newsListViewModel = ViewModelProviders.of(this).get(NewsListViewModel.class);
        newsListViewModel.attachView(this);
        if (sourceInfo != null) {
            newsListViewModel.applyNewsSourceInfo(sourceInfo);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        newsListViewModel.detachView();
    }

    @Override
    public void draw(@NonNull List<NewsInfo> newsInfo) {
        adapter.setNewsInfo(newsInfo);
        adapter.notifyDataSetChanged();
    }
}
