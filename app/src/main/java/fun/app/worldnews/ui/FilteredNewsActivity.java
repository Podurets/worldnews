package fun.app.worldnews.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import fun.app.worldnews.Constants;
import fun.app.worldnews.R;
import fun.app.worldnews.adapters.ClickHolder;
import fun.app.worldnews.adapters.NewsAdapter;
import fun.app.worldnews.adapters.SpacesItemDecoration;
import fun.app.worldnews.models.NewsInfo;
import fun.app.worldnews.models.NewsResult;
import fun.app.worldnews.utils.Utils;
import fun.app.worldnews.vm.FilteredNewsViewModel;
import fun.app.worldnews.vm.IFilteredNewsView;

public class FilteredNewsActivity extends AppCompatActivity implements IFilteredNewsView {

    private ChipGroup chipGroup;
    private RecyclerView recyclerViewNews;
    private EditText filterEditText;
    private Button applyFilterButton;
    private TextView missingFiltersTextView;

    private AdView adView;

    private FilteredNewsViewModel filteredNewsViewModel;
    private final NewsAdapter adapter = new NewsAdapter();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filtered_news_layout);
        chipGroup = findViewById(R.id.chipGroup);
        recyclerViewNews = findViewById(R.id.recyclerViewNews);
        filterEditText = findViewById(R.id.filterEditText);
        applyFilterButton = findViewById(R.id.applyFilterButton);
        missingFiltersTextView = findViewById(R.id.missingFiltersTextView);

        recyclerViewNews.setHasFixedSize(false);
        recyclerViewNews.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        ColorDrawable drawable = new ColorDrawable();
        drawable.setColor(Color.BLACK);
        drawable.setBounds(0, 0, 1, 5);
        dividerItemDecoration.setDrawable(drawable);
        recyclerViewNews.addItemDecoration(dividerItemDecoration);
        recyclerViewNews.addItemDecoration(new SpacesItemDecoration((int) Utils.dpToPx(5)));
        recyclerViewNews.setAdapter(adapter);
        adapter.setClickListenerAdapter(new ClickHolder.ClickListener() {
            @Override
            public void onClick(int position) {
                NewsInfo newsInfo = adapter.getNewsInfo(position);
                if (newsInfo != null && newsInfo.getUrl() != null) {
                    Intent intent = new Intent(FilteredNewsActivity.this, BrowserActivity.class);
                    intent.putExtra(Constants.TITLE_KEY, newsInfo.getTitle());
                    intent.putExtra(Constants.URL_KEY, newsInfo.getUrl());
                    RecyclerView.ViewHolder viewHolder = recyclerViewNews.findViewHolderForAdapterPosition(position);
                    if (viewHolder instanceof NewsAdapter.Holder) {
                        startActivity(intent,
                                ActivityOptionsCompat.makeSceneTransitionAnimation(FilteredNewsActivity.this,
                                        ((NewsAdapter.Holder) viewHolder).getTransitionView(),
                                        Constants.TRANSITION_NAME)
                                        .toBundle());
                    } else {
                        startActivity(intent);
                    }
                }
            }
        });
        filteredNewsViewModel = ViewModelProviders.of(this).get(FilteredNewsViewModel.class);
        filteredNewsViewModel.attachView(this);
        filteredNewsViewModel.obtainFilters();
        applyFilterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Editable editable = filterEditText.getText();
                if (editable != null) {
                    String text = editable.toString().trim();
                    if(!TextUtils.isEmpty(text)){
                        filterEditText.setText("");
                        filteredNewsViewModel.addFilter(text);
                    }
                }
            }
        });

        adView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        adView.loadAd(adRequest);
    }

    @Override
    protected void onStart() {
        super.onStart();
        filteredNewsViewModel.loadNews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        adView.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adView.destroy();
    }

    @NonNull
    private Chip buildChip(@NonNull String chipText) {
        Chip chip = new Chip(this);
        chip.setText(chipText);
        chip.setClickable(true);
        chip.setCloseIconVisible(true);
        chip.setCloseIconResource(R.drawable.ic_close);
        chip.setCheckable(false);
        chip.setOnCloseIconClickListener(chipOnClickListener);
        return chip;
    }

   private View.OnClickListener chipOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v instanceof Chip) {
                CharSequence text = ((Chip) v).getText();
                if (text != null) {
                    filteredNewsViewModel.removeFilter(text.toString());
                }
            }
        }
    };

    @Override
    public void drawFilters(@NonNull List<String> filters) {
        chipGroup.removeAllViews();
        for (String filter : filters) {
            chipGroup.addView(buildChip(filter));
        }
    }

    @Override
    public void addDrawFilter(@NonNull String filter) {
        chipGroup.addView(buildChip(filter));
    }

    @Override
    public void drawNews(@NonNull NewsResult newsResult) {
        adapter.setNewsInfo(newsResult.getResult() == null ?
                new ArrayList<NewsInfo>() : newsResult.getResult());
        adapter.notifyDataSetChanged();
    }

    @Nullable
    @Override
    public Context getContext() {
        return this.getApplicationContext();
    }
}
