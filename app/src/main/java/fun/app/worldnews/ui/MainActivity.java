package fun.app.worldnews.ui;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

import fun.app.worldnews.Constants;
import fun.app.worldnews.R;
import fun.app.worldnews.adapters.ClickHolder;
import fun.app.worldnews.adapters.NewsAdapter;
import fun.app.worldnews.adapters.NewsCategoriesAdapter;
import fun.app.worldnews.adapters.SpacesItemDecoration;
import fun.app.worldnews.models.NewsCategory;
import fun.app.worldnews.models.NewsInfo;
import fun.app.worldnews.models.NewsResult;
import fun.app.worldnews.models.NewsSourceInfo;
import fun.app.worldnews.models.SourceInfoType;
import fun.app.worldnews.ui.modules.common.AdsActivity;
import fun.app.worldnews.ui.modules.create.CreateNewsSourceActivity;
import fun.app.worldnews.ui.modules.sources.NewsSourcesActivity;
import fun.app.worldnews.utils.Utils;
import fun.app.worldnews.vm.IMainView;
import fun.app.worldnews.vm.MainViewModel;

public class MainActivity extends AdsActivity implements IMainView {

    private final NewsAdapter adapter = new NewsAdapter();
    NewsCategoriesAdapter categoriesAdapter;
    private MainViewModel mainViewModel;

    private AdView adView;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        categoriesAdapter = new NewsCategoriesAdapter(this);
        final RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        ColorDrawable drawable = new ColorDrawable();
        drawable.setColor(Color.BLACK);
        drawable.setBounds(0, 0, 1, 5);
        dividerItemDecoration.setDrawable(drawable);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.addItemDecoration(new SpacesItemDecoration((int) Utils.dpToPx(5)));
        recyclerView.setAdapter(adapter);
        adapter.setClickListenerAdapter(new ClickHolder.ClickListener() {
            @Override
            public void onClick(int position) {
                NewsInfo newsInfo = adapter.getNewsInfo(position);
                if (newsInfo != null && newsInfo.getUrl() != null) {
                    Intent intent = new Intent(MainActivity.this, BrowserActivity.class);
                    intent.putExtra(Constants.TITLE_KEY, newsInfo.getTitle());
                    intent.putExtra(Constants.URL_KEY, newsInfo.getUrl());
                    RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(position);
                    if (viewHolder instanceof NewsAdapter.Holder) {
                        startActivity(intent,
                                ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this,
                                        ((NewsAdapter.Holder) viewHolder).getTransitionView(),
                                        Constants.TRANSITION_NAME)
                                        .toBundle());
                    } else {
                        startActivity(intent);
                    }
                }
            }
        });
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        mainViewModel.attachView(this);

        adView = findViewById(R.id.adView);
        View fabView = findViewById(R.id.fab);
        fabView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, NewsSourcesActivity.class));
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mainViewModel.loadNews();
    }

    @NonNull
    @Override
    protected AdView getAdsView() {
        return adView;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.news_types_menu, menu);

        MenuItem item = menu.findItem(R.id.spinnerCategoryMenuItem);
        spinner = (Spinner) item.getActionView();
        categoriesAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(categoriesAdapter);
        spinner.setSelection(categoriesAdapter.getSelectedIndex(mainViewModel.getNewsCategory()));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                NewsCategory category = categoriesAdapter.getCategory(position);
                mainViewModel.loadNews(category);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filterMenuItem:
                startActivity(new Intent(this, FilteredNewsActivity.class));
                return true;
            case R.id.addItem: {
                startActivity(new Intent(this, CreateNewsSourceActivity.class));
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void drawNews(@NonNull NewsCategory newsCategory, @NonNull NewsResult newsResult) {
        adapter.setNewsInfo(newsResult.getResult() == null ?
                new ArrayList<NewsInfo>() : newsResult.getResult());
        adapter.notifyDataSetChanged();
        if (spinner != null) {
            if (spinner.getSelectedItemPosition() != categoriesAdapter.getSelectedIndex(newsCategory)) {
                spinner.setSelection(categoriesAdapter.getSelectedIndex(newsCategory));
            }
        }
    }
}
