package fun.app.worldnews.ui.modules.sources;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import fun.app.worldnews.R;
import fun.app.worldnews.adapters.ArraysAdapter;
import fun.app.worldnews.adapters.ClickHolder;
import fun.app.worldnews.interfaces.INewsSourceInfo;

public class SourcesAdapter extends ArraysAdapter<INewsSourceInfo, SourcesAdapter.SourcesHolder> {

    private @Nullable ClickHolder.ClickListener clickListenerAdapter;

    @NonNull
    @Override
    public SourcesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return SourcesHolder.create(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull SourcesHolder holder, int position) {
        INewsSourceInfo sourceInfo = get(position);
        if (sourceInfo != null) {
            holder.tvTitle.setText(sourceInfo.getTitle());
            if (holder.ivIcon.getDrawable() == null) {
                Glide.with(holder.ivIcon).load(R.drawable.news_icon).into(holder.ivIcon);
            }
        }
        holder.setClickListener(clickListenerAdapter);
    }


    @Override
    public void onViewAttachedToWindow(@NonNull SourcesHolder holder) {
        super.onViewAttachedToWindow(holder);
    }

    public void setClickListenerAdapter(@Nullable ClickHolder.ClickListener clickListenerAdapter) {
        this.clickListenerAdapter = clickListenerAdapter;
        if (getItemCount() > 0) {
            notifyDataSetChanged();
        }
    }


    static class SourcesHolder extends ClickHolder {

        final ImageView ivIcon;
        final TextView tvTitle;

        private SourcesHolder(@NonNull View itemView) {
            super(itemView);
            ivIcon = itemView.findViewById(R.id.iv_icon);
            tvTitle = itemView.findViewById(R.id.tv_title);
        }

        @NonNull
        static SourcesHolder create(@NonNull ViewGroup parent) {
            return new SourcesHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.sources_item, parent, false));
        }
    }
}
