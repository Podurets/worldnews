package fun.app.worldnews.ui.modules.common;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public abstract class AdsActivity extends AppCompatActivity {

    @NonNull
    protected abstract AdView getAdsView();

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        getAdsView().loadAd(new AdRequest.Builder().build());
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAdsView().resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getAdsView().pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getAdsView().destroy();
    }
}
