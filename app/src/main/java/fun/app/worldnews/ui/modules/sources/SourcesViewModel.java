package fun.app.worldnews.ui.modules.sources;

import androidx.annotation.NonNull;

import java.util.List;

import fun.app.worldnews.engine.NewsSourceManager;
import fun.app.worldnews.interfaces.Callback;
import fun.app.worldnews.interfaces.INewsSourceInfo;
import fun.app.worldnews.vm.AViewModel;

public class SourcesViewModel extends AViewModel<ISourcesView> {

    @NonNull
    private final NewsSourceManager newsSourceManager = new NewsSourceManager();

    @NonNull
    private final Callback<List<INewsSourceInfo>> callback = new Callback<List<INewsSourceInfo>>() {
        @Override
        public void onResult(List<INewsSourceInfo> iNewsSourceList) {
            ISourcesView view = view();
            if (view != null) {
                view.draw(iNewsSourceList);
            }
        }
    };

    @Override
    public void attachView(@NonNull ISourcesView view) {
        super.attachView(view);
        newsSourceManager.subscribe(callback);
        newsSourceManager.load();
    }

    @Override
    public void detachView() {
        newsSourceManager.unsubscribe(callback);
        super.detachView();
    }

    @Override
    protected void onCleared() {
        newsSourceManager.clearCache();
        super.onCleared();
    }
}
