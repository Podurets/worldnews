package fun.app.worldnews.ui.modules.create;

import androidx.annotation.NonNull;

import fun.app.worldnews.engine.rss.RssFeedParser;
import fun.app.worldnews.vm.IView;

public interface ICreateNewSourceView extends IView {

    void drawError(@NonNull CharSequence error);
    void draw(@NonNull RssFeedParser.RssInfo info);

}
