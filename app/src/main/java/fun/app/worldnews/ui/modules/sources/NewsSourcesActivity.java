package fun.app.worldnews.ui.modules.sources;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdView;

import java.util.List;

import fun.app.worldnews.R;
import fun.app.worldnews.adapters.ClickHolder;
import fun.app.worldnews.interfaces.INewsSourceInfo;
import fun.app.worldnews.ui.NewsListActivity;
import fun.app.worldnews.ui.modules.common.AdsActivity;

public class NewsSourcesActivity extends AdsActivity implements ISourcesView {

    private AdView adView;
    @NonNull
    private final SourcesAdapter adapter = new SourcesAdapter();
    private SourcesViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_sources_layout);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        adapter.setClickListenerAdapter(new ClickHolder.ClickListener() {
            @Override
            public void onClick(int position) {
                INewsSourceInfo sourceInfo = adapter.get(position);
                if (sourceInfo != null) {
                    NewsListActivity.startNewsListActivity(NewsSourcesActivity.this, sourceInfo);
                }
            }
        });
        adView = findViewById(R.id.adView);
        viewModel = ViewModelProviders.of(this).get(SourcesViewModel.class);
        viewModel.attachView(this);
    }

    @Override
    protected void onDestroy() {
        viewModel.attachView(this);
        super.onDestroy();
    }

    @NonNull
    @Override
    protected AdView getAdsView() {
        return adView;
    }

    @Override
    public void draw(@NonNull List<INewsSourceInfo> sourceInfoList) {
        adapter.apply(sourceInfoList);
        adapter.notifyDataSetChanged();
    }
}
