package fun.app.worldnews.ui.modules.create;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.ads.AdView;

import fun.app.worldnews.R;
import fun.app.worldnews.engine.rss.RssFeedParser;
import fun.app.worldnews.ui.modules.common.AdsActivity;

public class CreateNewsSourceActivity extends AdsActivity implements ICreateNewSourceView {

    private AdView adView;
    private CreateNewSourceViewModel viewModel;
    private EditText rssLinkEditText;
    private View fetchButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_news_source_layout);
        rssLinkEditText = findViewById(R.id.rssLinkEditText);
        adView = findViewById(R.id.adView);
        viewModel = ViewModelProviders.of(this).get(CreateNewSourceViewModel.class);
        viewModel.attachView(this);
        fetchButton = findViewById(R.id.fetchButton);
        fetchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchButton.setEnabled(false);
                viewModel.loadRssInfo(rssLinkEditText.getText().toString().trim());
            }
        });
        rssLinkEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                updateButtonByText(s);
            }
        });
        updateButtonByText(rssLinkEditText.getText());
    }

    private void updateButtonByText(Editable text) {
        if (text == null || text.toString().trim().length() == 0) {
            fetchButton.setEnabled(false);
        } else {
            fetchButton.setEnabled(true);
        }
    }


    @Override
    protected void onDestroy() {
        viewModel.detachView();
        super.onDestroy();
    }

    @NonNull
    @Override
    protected AdView getAdsView() {
        return adView;
    }

    @Override
    public void drawError(@NonNull CharSequence error) {
        updateButtonByText(rssLinkEditText.getText());
        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void draw(@NonNull RssFeedParser.RssInfo info) {
        updateButtonByText(rssLinkEditText.getText());
        Toast.makeText(getApplicationContext(), "loaded: "+info.getRssItems().size(), Toast.LENGTH_SHORT).show();
    }
}
