package fun.app.worldnews.ui.modules.create;

import androidx.annotation.NonNull;

import fun.app.worldnews.engine.IExtCallback;
import fun.app.worldnews.engine.RssInfoLoader;
import fun.app.worldnews.engine.Verifier;
import fun.app.worldnews.engine.rss.RssFeedParser;
import fun.app.worldnews.vm.AViewModel;

public class CreateNewSourceViewModel extends AViewModel<ICreateNewSourceView> {

    @NonNull
    private final RssInfoLoader loader = new RssInfoLoader();

    private IExtCallback<RssFeedParser.RssInfo> callback = new IExtCallback<RssFeedParser.RssInfo>() {
        @Override
        public void onError(@NonNull String error) {
            drawError(error);
        }

        @Override
        public void onResult(RssFeedParser.RssInfo rssInfo) {
            drawRssInfo(rssInfo);
        }
    };

    @Override
    public void attachView(@NonNull ICreateNewSourceView view) {
        super.attachView(view);
        loader.subscribe(callback);
    }

    @Override
    public void detachView() {
        super.detachView();
        loader.unsubscribe(callback);
    }

    void loadRssInfo(@NonNull String link) {
        if (link.isEmpty()) {
            drawError("Empty link url");
            return;
        }
        String url = Verifier.verifyUrl(link);
        loader.subscribe(callback);
        loader.applyLoadUrl(url);
        loader.load();
    }

    private void drawError(@NonNull String error) {
        ICreateNewSourceView view = view();
        if (view != null) {
            view.drawError(error);
        }
    }

    private void drawRssInfo(@NonNull RssFeedParser.RssInfo info) {
        ICreateNewSourceView view = view();
        if (view != null) {
            view.draw(info);
        }
    }

}
