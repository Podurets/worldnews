package fun.app.worldnews.ui.modules.sources;

import androidx.annotation.NonNull;

import java.util.List;

import fun.app.worldnews.interfaces.INewsSourceInfo;
import fun.app.worldnews.vm.IView;

interface ISourcesView extends IView {


    void draw(@NonNull List<INewsSourceInfo> sourceInfoList);

}
