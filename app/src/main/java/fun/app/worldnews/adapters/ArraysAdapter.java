package fun.app.worldnews.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public abstract class ArraysAdapter<Item, Holder extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<Holder> {

    @NonNull
    private final ArrayList<Item> items = new ArrayList<>();

    public void clear() {
        if (items.size() > 0) {
            items.clear();
            notifyDataSetChanged();
        }
    }

    public void apply(@NonNull List<Item> newItems) {
        items.clear();
        items.addAll(newItems);
        notifyDataSetChanged();
    }

    @Nullable
    public Item get(int index) {
        if (index >= 0 && items.size() > index) {
            return items.get(index);
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}