package fun.app.worldnews.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import fun.app.worldnews.R;
import fun.app.worldnews.models.NewsInfo;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.Holder> {

    @NonNull
    private final List<NewsInfo> newsInfoList = new ArrayList<>();
    @Nullable
    private ClickHolder.ClickListener clickListenerAdapter;

    public void setNewsInfo(@NonNull List<NewsInfo> newsInfo) {
        newsInfoList.clear();
        newsInfoList.addAll(newsInfo);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        NewsInfo newsInfo = newsInfoList.get(position);
        holder.titleTv.setText(newsInfo.getTitle());
        holder.descriptionTv.setText(newsInfo.getDescription());
        if (newsInfo.getUrlToImage() != null) {
            Glide.with(holder.icon).load(newsInfo.getUrlToImage()).placeholder(R.drawable.news_icon).into(holder.icon);
        } else {
            Glide.with(holder.itemView).load(R.drawable.news_icon).into(holder.icon);
        }
        holder.setClickListener(clickListenerAdapter);
    }

    public void setClickListenerAdapter(@Nullable ClickHolder.ClickListener clickListenerAdapter) {
        this.clickListenerAdapter = clickListenerAdapter;
        if (getItemCount() > 0) {
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return newsInfoList.size();
    }

    @Nullable
    public NewsInfo getNewsInfo(int position) {
        if (getItemCount() > position && position >= 0) {
            return newsInfoList.get(position);
        }
        return null;
    }

    public static class Holder extends ClickHolder {

        @NonNull
        private TextView titleTv;
        private TextView descriptionTv;
        private ImageView icon;

        private Holder(@NonNull LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.news_item, parent, false));
            titleTv = itemView.findViewById(R.id.titleTv);
            descriptionTv = itemView.findViewById(R.id.descriptionTv);
            icon = itemView.findViewById(R.id.iv_icon);
        }

        @NonNull
        public TextView getTransitionView() {
            return titleTv;
        }
    }

}
