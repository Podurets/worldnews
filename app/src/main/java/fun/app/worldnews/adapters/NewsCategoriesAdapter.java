package fun.app.worldnews.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import fun.app.worldnews.R;
import fun.app.worldnews.models.NewsCategory;

public class NewsCategoriesAdapter extends ArrayAdapter<CharSequence> {

    @NonNull
    private final List<NewsCategory> newsCategories = new ArrayList<>(Arrays.asList(NewsCategory.values()));
    private LayoutInflater inflater;

    public NewsCategoriesAdapter(@NonNull Context context) {
        super(context, R.layout.simple_spinner_item);
        inflater = LayoutInflater.from(context);
    }

    public NewsCategory getCategory(int position) {
        return newsCategories.get(position);
    }

    public int getSelectedIndex(@NonNull NewsCategory category) {
        for (int i = 0; i < newsCategories.size(); i++) {
            if (newsCategories.get(i).ordinal() == category.ordinal()) {
                return i;
            }
        }
        return 0;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final View view;
        final TextView text;
        if (convertView == null) {
            view = inflater.inflate(R.layout.simple_spinner_item, parent, false);
        } else {
            view = convertView;
        }
        text = (TextView) view;
        text.setText(getItem(position));
        return view;
    }

    @Override
    public int getCount() {
        return newsCategories.size();
    }

    @Nullable
    @Override
    public CharSequence getItem(int position) {
        return NewsCategory.getText(newsCategories.get(position), getContext());
    }
}
