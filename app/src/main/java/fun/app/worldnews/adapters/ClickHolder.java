package fun.app.worldnews.adapters;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

public abstract class ClickHolder extends RecyclerView.ViewHolder {

    public ClickHolder(@NonNull View itemView) {
        super(itemView);
    }

    public static abstract class ClickListener {
        public abstract void onClick(int position);
    }

    @Nullable
    private ClickListener clickListener;

    public void setClickListener(@Nullable ClickListener clickListener) {
        this.clickListener = clickListener;
        itemView.setOnClickListener(clickListener == null ? null : clickListenerHolder);
    }

    private View.OnClickListener clickListenerHolder = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                int position = getAdapterPosition();
                if (position != NO_POSITION) {
                    clickListener.onClick(position);
                }
            }
        }
    };
}
