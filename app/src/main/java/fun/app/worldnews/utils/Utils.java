package fun.app.worldnews.utils;

import android.content.res.Resources;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

public abstract class Utils {

    public static void applyTransitionAnimation(@Nullable Toolbar toolBar, @NonNull String transitionName){
        if(toolBar!=null){
            TextView textView = getTextViewTitle(toolBar);
            if (textView != null) {
                textView.setTransitionName(transitionName);
            }
        }
    }


    @Nullable
    public static TextView getTextViewTitle(Toolbar toolbar) {
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                return (TextView) view;
            }
        }
        return null;
    }

    public static float dpToPx(int dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                Resources.getSystem().getDisplayMetrics());
    }

}
