package fun.app.worldnews;

public interface Constants {

    String NEWS_API_ACCESS_KEY = "b0d86a4f2a8e4043aee20c1c7e2e6a41";
    int NEWS_ON_PAGE = 100;

    String TRANSITION_NAME = "Anim";
    String TITLE_KEY = "tit";
    String URL_KEY = "url";

    String INTENT_KEY = "int_key";

}
