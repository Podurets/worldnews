package fun.app.worldnews.repo;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AppDataRepository {

    private static final String FILTER_PREF_FILE_NAME = "filters";

    public static void saveFilters(@NonNull Context context, @Nullable List<String> filters){
       SharedPreferences preferences = context.getSharedPreferences(FILTER_PREF_FILE_NAME, Context.MODE_PRIVATE);
       if(filters == null || filters.size() == 0){
           preferences.edit().remove(FILTER_PREF_FILE_NAME).apply();
       } else {
           Set<String> filtersSet = new HashSet<>(filters);
           preferences.edit().putStringSet(FILTER_PREF_FILE_NAME, filtersSet).apply();
       }
    }

    @NonNull
    public static List<String> getSavedFilters(@NonNull Context context){
        SharedPreferences preferences = context.getSharedPreferences(FILTER_PREF_FILE_NAME, Context.MODE_PRIVATE);
        Set<String> filtersSet = preferences.getStringSet(FILTER_PREF_FILE_NAME, null);
        ArrayList<String> filters = new ArrayList<>();
        if (filtersSet != null && filtersSet.size() > 0) {
            filters.addAll(filtersSet);
        }
        return filters;
    }

}
