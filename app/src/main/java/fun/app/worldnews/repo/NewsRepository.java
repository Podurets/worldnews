package fun.app.worldnews.repo;

import android.text.TextUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import fun.app.worldnews.R;
import fun.app.worldnews.interfaces.Callback;
import fun.app.worldnews.models.NewsApiResponse;
import fun.app.worldnews.models.NewsCategory;
import fun.app.worldnews.models.NewsResult;
import fun.app.worldnews.net.RetrofitClient;
import retrofit2.Call;
import retrofit2.Response;

public class NewsRepository {

    @Nullable
    private SimpleDateFormat serverDateFormat;

    @NonNull
    private SimpleDateFormat getFormat() {
        if (serverDateFormat == null) {
            serverDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        }
        return serverDateFormat;
    }


    @NonNull
    private NewsResult parse(Response<NewsApiResponse> response) {
        NewsApiResponse newsApiResponse = response.body();
        NewsResult newsResult = new NewsResult(response.isSuccessful());
        newsResult.setCode(response.code());
        if (newsApiResponse != null) {
            newsResult.applyResult(newsApiResponse.getNewsInfoList());
        } else {
            newsResult.setMessage(response.message());
        }
        return newsResult;
    }

    public static String getCategoryParam(@NonNull NewsCategory category){
        switch (category){
            case ALL:
                return "";
            case BUSINESS:
                return "business";
            case ENTERTAINMENT:
                return "entertainment";
            case GENERAL:
                return "general";
            case HEALTH:
                return "health";
            case SCIENCE:
                return "science";
            case SPORTS:
                return "sports";
            case TECHNOLOGY:
                return "technology";
        }
        return "";
    }

    @NonNull
    public Call<NewsApiResponse>  loadNews(@NonNull NewsCategory category, @NonNull final Callback<NewsResult> callback) {
        /* business entertainment general health science sports technology */
        String categoryParam = getCategoryParam(category);
        Call<NewsApiResponse> call;
        if (TextUtils.isEmpty(categoryParam)) {
            call = RetrofitClient.get().service().topHeadLines("ua");
        } else {
            call = RetrofitClient.get().service().topHeadLines("ua", categoryParam);
        }
        call.enqueue(new retrofit2.Callback<NewsApiResponse>() {
            @Override
            public void onResponse(Call<NewsApiResponse> call, Response<NewsApiResponse> response) {
                callback.onResult(parse(response));
            }

            @Override
            public void onFailure(Call<NewsApiResponse> call, Throwable t) {
                callback.onResult(new NewsResult(false, t.getMessage()));
            }
        });
        return call;
    }

    public void loadNews(@NonNull ArrayList<String> newsKeywordList, long dateFrom, @NonNull final Callback<NewsResult> callback) {
        if (newsKeywordList.size() == 0) {
            callback.onResult(new NewsResult(false, "Missing topic keywords"));
            return;
        }
        StringBuilder query = new StringBuilder();
        for (String keyWord : newsKeywordList) {
            if (query.length() != 0) {
                query.append(" AND ");
            }
            query.append(keyWord);
        }
        try {
           // String keyQuery = URLEncoder.encode(query.toString(), "UTF-8");
            String keyQuery = query.toString().replaceAll(" ", "%20");
            RetrofitClient.get().service().getTopicNews(keyQuery, getFormat().format(new Date(dateFrom)))
                    .enqueue(new retrofit2.Callback<NewsApiResponse>() {
                @Override
                public void onResponse(Call<NewsApiResponse> call, Response<NewsApiResponse> response) {
                    callback.onResult(parse(response));
                }

                @Override
                public void onFailure(Call<NewsApiResponse> call, Throwable t) {
                    callback.onResult(new NewsResult(false,t.getMessage()));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
